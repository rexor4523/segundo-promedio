﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_PowerUp2 : MonoBehaviour
{
    Vista_PowerUp2 vistaPoweUp2;

    private void Start()
    {
        vistaPoweUp2 = GetComponent<Vista_PowerUp2>();

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("PowerUp");
            vistaPoweUp2.CogerPU2(other);
        }
    }
}
