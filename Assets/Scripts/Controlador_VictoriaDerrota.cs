﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_VictoriaDerrota : MonoBehaviour
{

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Derrota")
        {
            Debug.Log("Has muerto");
            Destroy(gameObject);
        }
    }
}
