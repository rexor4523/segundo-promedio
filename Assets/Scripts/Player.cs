﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Rigidbody2D Myrigidbody;

    [SerializeField]
    private float movementSpeed;

    public float jumpForce;
    private float moveInput;

    private bool canJump;

    void Start()
    {

        Myrigidbody = GetComponent<Rigidbody2D>();

    }

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");

        HandleMovement(horizontal);
    }

    private void HandleMovement(float horizontal)
    {
        Myrigidbody.velocity = new Vector2(horizontal*movementSpeed, Myrigidbody.velocity.y);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.W) && canJump == true)
        {
            Myrigidbody.velocity = Vector2.up * jumpForce;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Ground")
        {
            canJump = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
       if(collision.gameObject.tag == "Ground")
        {
            canJump = false;
        }
    }

}
