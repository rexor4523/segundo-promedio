﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vista_PowerUp : MonoBehaviour
{
    public float grande = 1.05f;
    public void Coger(Collider2D col)
    {
        Debug.Log("PoweredUP");

        col.transform.localScale *= grande;
        Destroy(gameObject);
    }
}
