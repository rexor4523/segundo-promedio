﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vista_PowerUp2 : MonoBehaviour
{
    public float speed = 10f;
    public void CogerPU2(Collider2D col)
    {
        Debug.Log("PoweredUP");

        col.transform.localScale *= speed;
        Destroy(gameObject);
    }
}
