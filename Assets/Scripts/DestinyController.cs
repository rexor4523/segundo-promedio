﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestinyController : MonoBehaviour
{
    public GameObject winText;
    public GameObject loseText;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Lose")
        {
            loseText.SetActive(true);
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Winn")
        {
            winText.SetActive(true);
        }
    }
}
