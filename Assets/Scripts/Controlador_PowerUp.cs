﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador_PowerUp : MonoBehaviour
{

    Vista_PowerUp vistaPoweUp;

     void Start()
    {
        vistaPoweUp = GetComponent<Vista_PowerUp>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("PowerUp");
            vistaPoweUp.Coger(other);
        }
    }

}
